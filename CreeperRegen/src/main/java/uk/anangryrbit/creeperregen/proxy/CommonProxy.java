package uk.anangryrbit.creeperregen.proxy;

import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.world.World;
import net.minecraftforge.event.world.ExplosionEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import uk.anangryrbit.creeperregen.utility.LogHelper;

/**
 * Created by AnAngryBrit on 24/02/2015.
 */
public class CommonProxy
{
    @SubscribeEvent
    public void onExplosion(ExplosionEvent event, EntityCreeper e,World world, Entity entity)
    {
        LogHelper.info("DAM CREEPER");
    }
}
